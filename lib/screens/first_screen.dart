import 'package:flutter/material.dart';
import 'package:re_word/screens/pushed_screen.dart';
import 'package:re_word/screens/third_screen.dart';

class FirstScreen extends StatefulWidget {
  FirstScreen({Key key, this.title}) : super(key: key);

  final String title;
  static const route = '/first';
  static int learned = 0;

  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.orangeAccent,
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Image.asset('images/study.jpg'),
                  title: Text('Beginner '),
                  subtitle: Text(
                    'Words learned : ' + FirstScreen.learned.toString(),
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                ),
              ],
            ),
          ),
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Image.asset('images/book.jpg'),
                  title: Text(FirstScreen.learned.toString()),
                  subtitle: Text(
                    'words you are learning now',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.start,
                  children: [
                    FlatButton(
                      onPressed: () {
                        Navigator.of(
                          context,
                          rootNavigator: true,
                        ).pushNamed(PushedScreen.route);
                        PushedScreen.instruct = true;
                      },
                      textColor: Colors.orangeAccent,
                      child: const Text('Instruction'),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(
                          context,
                          rootNavigator: false,
                        ).pushNamed(ThirdScreen.route);
                      },
                      textColor: Colors.white,
                      color: Colors.orangeAccent,
                      child: const Text('Start Learning'),
                    ),
                  ],
                ),
                Image.asset(
                  'images/pen.jpg',
                  scale: 2.5,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
