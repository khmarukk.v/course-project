import 'package:flutter/material.dart';

class PushedScreen extends StatelessWidget {
  static const route = '/first/pushed';
  static String category = '';
  static bool instruct = false;
  static Map<String, String> wordMap = new Map<String, String>();
  List<String> words = new List();

  void fillMap(String category) {
    if (category == "Animals") {
      wordMap = {
        'bear': 'медведь',
        'ferret': 'хорёк',
        'hare': 'заяц',
        'gull': 'чайка'
      };
    } else if (category == 'Appearance') {
      wordMap = {
        'beard': 'борода',
        'brows': 'брови',
        'slim': 'худой',
        'wrist': 'запястье'
      };
    } else if (category == 'Family') {
      wordMap = {
        'bride': 'невеста',
        'engagement': 'помолвка',
        'twin': 'близнец',
        'divorce': 'развод'
      };
    } else if (category == 'Home') {
      wordMap = {
        'attic': 'чердак',
        'bath': 'ванна',
        'bench': 'скамейка',
        'fence': 'забор'
      };
    } else if (category == 'Health') {
      wordMap = {
        'ambulance': 'скорая помощь',
        'back': 'спина',
        'bone': 'кость',
        'ache': 'боль'
      };
    }
  }


void formList(Map wordMap) {
  for (String str in wordMap.keys) {
    words.add(str);
  }
}

@override
Widget build(BuildContext context) {
  fillMap(category);
  formList(wordMap);
  if (category != '') {
    print(words);
    return Scaffold(
        appBar: AppBar(
          title: Text(category),
          backgroundColor: Colors.orangeAccent,
        ),
        body: ListView.separated(
            itemCount: wordMap.keys.length,
            itemBuilder: (BuildContext context, int index) {
              return new Card(
                child: new Container(
                  padding: new EdgeInsets.all(10.0),
                  child: new Column(
                    children: <Widget>[
                      ListTile(
                        title: Text(words[index]),
                        subtitle: Text(wordMap[words[index]],
                          style: TextStyle(color: Colors.black.withOpacity(
                              0.6)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context,
                int index) => const Divider()));
  } else if (instruct) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Instructions'),
        backgroundColor: Colors.orangeAccent,
      ),
      body: Center(
          child: Text(
            'Choose word categories in Category tab and start learning process in Learn tab. ',
            textAlign: TextAlign.center,
          )),
    );
  }
}}
