import 'package:flutter/material.dart';
import 'package:re_word/providers/navigation_provider.dart';
import 'package:provider/provider.dart';

/// Navigation entry point for app.
class Root extends StatelessWidget {
  static const route = '/';

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationProvider>(
      builder: (context, provider, child) {
        // Create bottom navigation bar items from screens.
        final bottomNavigationBarItems = provider.screens
            .map((screen) => BottomNavigationBarItem(
                icon: screen.icon,
                title: Text(screen.title),
                backgroundColor: Colors.orangeAccent))
            .toList();
        final screens = provider.screens
            .map(
              (screen) => Navigator(
                key: screen.navigatorState,
                onGenerateRoute: screen.onGenerateRoute,
              ),
            )
            .toList();

        return WillPopScope(
          onWillPop: () async => provider.onWillPop(context),
          child: Scaffold(
            body: IndexedStack(
              children: screens,
              index: provider.currentTabIndex,
            ),
            bottomNavigationBar: BottomNavigationBar(
              items: bottomNavigationBarItems,
             // backgroundColor: Colors.orangeAccent,
              currentIndex: provider.currentTabIndex,
              fixedColor: Colors.orangeAccent,
              onTap: provider.setTab,
            ),
          ),
        );
      },
    );
  }
}
