import 'package:flutter/material.dart';
import 'package:re_word/providers/navigation_provider.dart';
import 'package:re_word/screens/first_screen.dart';
import 'package:re_word/screens/pushed_screen.dart';
import 'package:re_word/screens/third_screen.dart';

class SecondScreen extends StatefulWidget {
  SecondScreen({Key key}) : super(key: key);

  static const route = '/second';

  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  static const route = '/second';
  Map<String, bool> categoriesBoolMap = {
    'Appearance': false,
    'Health': false,
    'Food': false,
    'Animals': false,
    'Home': false,
    'Family': false
  };
  List<String> categories = [
    'Appearance',
    'Health',
    'Food',
    'Animals',
    'Home',
    'Family'
  ];
  List<String> chosenCategories = new List<String>();

  Map<String, String> categoriesImageMap = {
    'Appearance': 'images/appearance.png',
    'Health': 'images/health.png',
    'Food': 'images/food.png',
    'Animals': 'images/hippo.png',
    'Home': 'images/house.png',
    'Family': 'images/family.png'
  };

  void handleCheckbox(bool val, int index) {
    setState(() {
      categoriesBoolMap[categories[index]] = val;
      chosenCategories.add(categories[index]);
      ThirdScreen.categories.add(categories[index]);
      FirstScreen.learned = categories.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Available Categories'),
        backgroundColor: Colors.orangeAccent,
      ),
      body: ListView.separated(
        itemCount: categories.length,
        controller: NavigationProvider.of(context)
            .screens[SECOND_SCREEN]
            .scrollController,
        itemBuilder: (BuildContext context, int index) {
          return new Card(
            child: new Container(
              padding: new EdgeInsets.all(10.0),
              child: new Column(
                children: <Widget>[
                  new CheckboxListTile(
                      value: categoriesBoolMap[categories[index]],
                      title: new FlatButton(
                        onPressed: () {
                          Navigator.of(
                            context,
                            rootNavigator: true,
                          ).pushNamed(PushedScreen.route);
                          PushedScreen.category = categories[index];
                        },
                        textColor: Colors.orangeAccent,
                        child: Text(categories[index]),
                      ),
                      controlAffinity: ListTileControlAffinity.leading,
                      onChanged: (bool val) {
                        handleCheckbox(val,index);
                      },
                      secondary:
                          Image.asset(categoriesImageMap[categories[index]])),
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    );
  }
}
