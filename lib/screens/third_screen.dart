import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class ThirdScreen extends StatelessWidget {
  static const route = '/third';

  static List<String> categories = new List<String>();

  Map<String, String> appearance = {
    'beard': 'images/beard.jpg',
    'brows': 'images/brows.jfif',
    'slim': 'images/slim.jpg',
    'wrist': 'images/wrist.jpeg'
  };

  Map<String, String> appearanceTranslate = {
    'beard': 'борода',
    'brows': 'брови',
    'slim': 'худой',
    'wrist': 'запястье'
  };

  Map<String, String> family = {
    'bride': 'images/bride.jpg',
    'engagement': 'images/engagement.jpg',
    'twin': 'images/twins.jfif',
    'divorce': 'images/divorce.jpg'
  };

  Map<String, String> familyTranslate = {
    'bride': 'невеста',
    'engagement': 'помолвка',
    'twin': 'близнец',
    'divorce': 'развод'
  };

  Map<String, String> homeTranslate = {
    'attic': 'чердак',
    'bath': 'ванна',
    'bench': 'скамейка',
    'fence': 'забор'
  };

  Map<String, String> home = {
    'attic': 'images/attic.jpg',
    'bath': 'images/bath.jpg',
    'bench': 'images/bench.jpg',
    'fence': 'images/fence.jpg'
  };

  Map<String, String> health = {
    'ambulance': 'images/am.png',
    'back': 'images/back.jpg',
    'bone': 'images/bone.jpg',
    'ache': 'images/ac.png'
  };
  Map<String, String> healthTranslate = {
    'ambulance': 'скорая помощь',
    'back': 'спина',
    'bone': 'кость',
    'ache': 'боль'
  };

  Map<String, String> animals = {
    'bear': 'images/bear.jpg',
    'ferret': 'images/ferret.jpg',
    'hare': 'images/hare.jpg',
    'gull': 'images/gull.jfif'
  };
  Map<String, String> animalsTranslation = {
    'bear': 'медведь',
    'ferret': 'хорёк',
    'hare': 'заяц',
    'gull': 'чайка'
  };
  Map<String, String> allWords = new Map<String, String>();
  Map<String, String> allWordsTranslation = new Map<String, String>();

  List<String> chosenWords = new List<String>();

  void selectWords(List<String> categories) {
    for (String category in categories) {
      if (category == 'Family') {
        allWords.addAll(family);
        allWordsTranslation.addAll(familyTranslate);
      }
      if (category == 'Appearance') {
        allWords.addAll(appearance);
        allWordsTranslation.addAll(appearanceTranslate);
      }
      if (category == 'Animals') {
        allWords.addAll(animals);
        allWordsTranslation.addAll(animalsTranslation);
      }
      if (category == 'Home') {
        allWords.addAll(home);
        allWordsTranslation.addAll(homeTranslate);
      }
      if (category == 'Health') {
        allWords.addAll(health);
        allWordsTranslation.addAll(healthTranslate);
      }
      chosenWords.addAll(allWords.keys);
    }
  }

  @override
  Widget build(BuildContext context) {
    selectWords(categories);
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.grey,
          body: Container(
            child: Swiper(
              itemCount: chosenWords.length,
              itemBuilder: (context, index) {
                return Card(
                  child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(allWords[chosenWords[index]]),
                          fit: BoxFit.fitWidth,
                          alignment: Alignment.bottomCenter,
                        ),
                      ),
                      child: Container(
                          child: Column(children: <Widget>[
                        Text('     ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                        Text(chosenWords[index],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                        Text(allWordsTranslation[chosenWords[index]],
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 24, color: Colors.grey)),
                        Text('     ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                        Text('     ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                      ]))),
                );
              },
            ),
          )),
    );
  }
}
